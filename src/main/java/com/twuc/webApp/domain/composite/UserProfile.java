package com.twuc.webApp.domain.composite;

// TODO
//
// 请补充 UserProfile 的定义以便通过所有的测试。UserProfile 所映射的 table 为 user_profile
//
// +─────────────+──────────────+───────────────────────────────+
// | column      | description  | additional                    |
// +─────────────+──────────────+───────────────────────────────+
// | id          | bigint       | auto_increment, primary key   |
// | first_name  | varchar(64)  | not null                      |
// | last_name   | varchar(64)  | not null                      |
// +─────────────+──────────────+───────────────────────────────+
//
// 你可以补充 annotation 也可以定义方法。
// <--start-

import javax.persistence.*;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Embedded
    private UserName userName;

    public UserProfile(UserName userName) {
        this.userName = userName;
    }

    public UserProfile() {
    }

    public void setUserName(UserName userName) {
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public UserName getUserName() {
        return userName;
    }
}
// --end->