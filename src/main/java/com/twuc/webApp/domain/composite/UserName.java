package com.twuc.webApp.domain.composite;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;

// TODO
//
// 补充 UserName 的定义，使 UserName 成为一个 value type。其中 firstName的列名称为 first_name，
// 而 lastName 的列名称为 last_name。两列都不能够为 null。并且 firstName 和 lastName 的最大长度
// 均为 64。
// <--start-
@Embeddable
public class UserName {
    @Column(nullable = false, length = 64)
    private String firstName;
    @Column(nullable = false, length = 64)
    private String lastName;

    public UserName(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserName() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
// --end->